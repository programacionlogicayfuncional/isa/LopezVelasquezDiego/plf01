(ns plf01.core)

(defn funcion-<-1
  [a b]
  (< a b))

(defn funcion-<-2
  [a b c]
  (< a b c))

(defn funcion-<-3
  [a b c d]
  (< (* a b) (+ c d)))

(funcion-<-1 10 12)
(funcion-<-2 14 16 17)
(funcion-<-3 7 8 10 4)

(defn funcion-<=-1
  [a b]
  (<= a b))

(defn funcion-<=-2
  [a b c]
  (<= a b c))

(defn funcion-<=-3
  [a b c d e f]
  (<= (+ a b) (* c d) (+ e f)))

(funcion-<=-1 5 6)
(funcion-<=-2 8 7 9)
(funcion-<=-3 5 7 8 9 1 6)

(defn funcion-==-1
  [x y]
  (== x y))

(defn funcion-==-2
  [x y z]
  (== x y z))

(defn funcion-==-3
  [a b c d]
  (== (+ a b) (+ c d)))

(funcion-==-1 1 1)
(funcion-==-2 8 7 4)
(funcion-==-3 6 4 7 3)

(defn funcion->-1
  [a b]
  (> a b))

(defn funcion->-2
  [a b c]
  (> a b c))

(defn funcion->-3
  [a b c d]
  (> (+ a b) (* c d)))

(funcion->-1 81 15)
(funcion->-2 15 71 65)
(funcion->-3 10 5 3 2)

(defn funcion->=-1
  [a b]
  (>= a b))

(defn funcion->=-2
  [a b c]
  (>= a b c))

(defn funcion->=-3
  [a b c d e f]
  (>= (* a b) (+ c d) (* e f)))

(funcion->=-1 8 7)
(funcion->=-2 15 71 23)
(funcion->=-3 7 8 1 4 5 6)

(defn funcion-assoc-1
 [a b]
  (assoc {} a b))

(defn funcion-assoc-2
  [a b]
  (assoc {} a b))

(defn funcion-assoc-3
  [a b]
  (assoc {} a b))

(funcion-assoc-1 \a \q)
(funcion-assoc-2 \a \b)
(funcion-assoc-3 5 9)

(defn funcion-assoc-in-1
  [a b c] 
  (assoc-in a b c))

(defn funcion-assoc-in-2
  [a b c] 
  (assoc-in a b c))

(defn funcion-assoc-in-3
  [a b c] 
  (assoc-in a b c))

(funcion-assoc-in-1 [5] [1] [6])

(defn funcion-concat-1
  [a] 
  (concat a))

(defn funcion-concat-2
  [a b] 
  (concat a b))

(defn funcion-concat-3
  [a b c] 
  (concat a b c))

(funcion-concat-1 [5])
(funcion-concat-2 [5] [8])
(funcion-concat-3 [5] [7] [9])

(defn funcion-conj-1
  [a] 
  (conj a))

(defn funcion-conj-2
  [a b] 
  (conj a b))

(defn funcion-conj-3
  [a b c] 
  (conj a b c))

(funcion-conj-1 [5])
(funcion-conj-2 [5] 5)
(funcion-conj-3 [5] [7] 4)

(defn funcion-cons-1
  [a b] 
  (cons a b))

(defn funcion-cons-2
  [a b] 
  (cons a b))

(defn funcion-cons-3
  [a b] 
  (cons a b))

(funcion-cons-1 [7] [8 ])
(funcion-cons-2 [9] [10])
(funcion-cons-3 [78] [80])

(defn funcion-contains?-1
  [a b] 
  (contains? a b))

(defn funcion-contains?-2
  [a b] 
  (contains? a b))

(defn funcion-contains?-3
  [a b] 
  (contains? a b))

(funcion-contains?-1 {:a 1} :a)
(funcion-contains?-2 {:b 1} :c)
(funcion-contains?-3 {:e 5} :e)

(defn funcion-count-1
  [a] 
  (count a))

(defn funcion-count-2
  [a] 
  (count a))

(defn funcion-count-3
  [a] 
  (count a))

(funcion-count-1 [])
(funcion-count-2 [1])
(funcion-count-3 [\a])

(defn funcion-disj-1
  [a] 
  (disj a))

(defn funcion-disj-2
  [a b] 
  (disj a b))

(defn funcion-disj-3
  [a b c] 
  (disj a b c))

(funcion-disj-1 #{1})
(funcion-disj-2 #{6} #{5})
(funcion-disj-3 #{8} #{7} 9)

(defn funcion-dissoc-1
  [a] 
  (dissoc a))

(defn funcion-dissoc-2
  [a b] 
  (dissoc a b))

(defn funcion-dissoc-3
  [a b c] 
  (dissoc a b c))

(funcion-dissoc-1 {:a 1})
(funcion-dissoc-2 {:a 3} :a)
(funcion-dissoc-3 {:a 7} {:b 9} :b)

(defn funcion-pop-1
  [a] 
  (pop a))

(defn funcion-pop-2
  [a] 
  (pop a))

(defn funcion-pop-3
  [a] 
  (pop a))

(funcion-pop-1 [5])
(funcion-pop-2 [5 7 8])
(funcion-pop-3 [5 8 7 9])

(defn funcion-distinct-1
  [a]
  (distinct a))
(defn funcion-distinct-2
  [a]
  (distinct a))
(defn funcion-distinct-3
  [a]
  (distinct a))

(funcion-distinct-1 [1 51 4])
(funcion-distinct-2 [1 14 4 51 4] )
(funcion-distinct-3 [5 8 7 4 2 6 4])

(defn funcion-distinct?-1
  [a]
  (distinct? a))
(defn funcion-distinct?-2
  [a b]
  (distinct? a b))
(defn funcion-distinct?-3
  [a b c]
  (distinct? a b c))

(funcion-distinct?-1 8)
(funcion-distinct?-2 8 8)
(funcion-distinct?-3 8 7 9)

(defn funcion-drop-last-1
  [a]
  (drop-last a))
(defn funcion-drop-last-2
  [a b]
  (drop-last b [a]))
(defn funcion-drop-last-3
  [a b ]
  (drop-last b #{a}))

(funcion-drop-last-1 [1 2 3])
(funcion-drop-last-2 [8 9 4] -1)
(funcion-drop-last-2 #{5 4 6} -1)

(defn funcion-empty-1
  [a]
  (empty a))
(defn funcion-empty-2
  [a]
  (empty a))
(defn funcion-empty-3
  [a]
  (empty a))

(funcion-empty-1 {1 8 })
(funcion-empty-2 [4 7])
(funcion-empty-3 #{7 9})

(defn funcion-empty?-1
  [a]
  (empty? a))
(defn funcion-empty?-2
  [a]
  (empty? a))
(defn funcion-empty?-3
  [a]
  (empty? a))

(funcion-empty?-1 [])
(funcion-empty?-2 [5])
(funcion-empty?-3 [4 8])

(defn funcion-even?-1
  [a]
  (even? a))
(defn funcion-even?-2
  [a]
  (even? a))
(defn funcion-even?-3
  [a]
  (even? a))

(funcion-even?-1 1)
(funcion-even?-2 8)
(funcion-even?-3 9)

(defn funcion-false?-1
  [a]
  (false? a))
(defn funcion-false?-2
  [a]
  (false? a))
(defn funcion-false?-3
  [a]
  (false? a))

(funcion-false?-1 true)
(funcion-false?-2 false)
(funcion-false?-3 nil)


(defn funcion-find-1
  [a b]
  (find a b ))
(defn funcion-find-2
  [a b ]
  (find a b ))
(defn funcion-find-3
  [a b]
  (find a b))

(funcion-find-1 {:a 1 :b 2 :c 3} :a)
(funcion-find-2 {:a nil} :a)
(funcion-find-3 {:a 1 :b 2 :c 3} :d)

(defn funcion-first-1
  [a]
  (first a))
(defn funcion-first-2
  [a]
  (first a))
(defn funcion-first-3
  [a]
  (first a))

(funcion-first-1 [\a \b \c])
(funcion-first-2 {\a \A \b \B \c \C})
(funcion-first-3 #{\a \b \c})

(defn funcion-flatten-1
  [a]
  (flatten a))
(defn funcion-flatten-2
  [a]
  (flatten a))
(defn funcion-flatten-3
  [a]
  (flatten a))
(funcion-flatten-1 nil)
(funcion-flatten-2 [2 6 4])
(funcion-flatten-3 {5 4})

(defn funcion-frequencies-1
  [a]
  (frequencies a))
(defn funcion-frequencies-2
  [a]
  (frequencies a))
(defn funcion-frequencies-3
  [a]
  (frequencies a))
(funcion-frequencies-1 nil)
(funcion-frequencies-2 ['a 'f' 's 'a])
(funcion-frequencies-3 [\a \c])

(defn funcion-get-1
  [a b]
  (get a b))
(defn funcion-get-2
  [a b c]
  (get a b c))
(defn funcion-get-3
  [a b c ]
  (get a b c))

(funcion-get-1 [1 2 3] 1)
(funcion-get-2 [1 2 3] 5 100)
(funcion-get-3 {:a 1 :b 2} :z "hola")

(defn funcion-get-in-1
  [a b]
  (get-in a b))
(defn funcion-get-in-2
  [a b]
  (get-in a b))
(defn funcion-get-in-3
  [a b]
  (get-in a b))
(funcion-get-in-1 {:nombre "jimmy"
                   :animal [{:nombre "Rex"
                           :raza :perro}
                          {:nombre "Sniffles"
                           :raza :hamster}]} [:animal 1])
(funcion-get-in-2 {:nombre "Ana"
                   :animal [{:nombre "kiara"
                             :raza :perro}
                            {:nombre "bingo"
                             :raza :hamster}]} [:animal 2])
(funcion-get-in-3 {:nombre "julio"
                   :animal [{:nombre "keisha"
                             :raza :perro}
                            {:nombre "kuki"
                             :raza :hamster}
                            {:nombre "scoby"
                             :raza :perico}]} [:animal 2])

(defn funcion-into-1
  [a b]
  (into a b))
(defn funcion-into-2
  [a]
  (into a))
(defn funcion-into-3
  [a b]
  (into a b))

(funcion-into-1 [] {1 2 3 4})
(funcion-into-2 [[:a 1] [:b 2] [:c 3]])
(funcion-into-2 [[1 2 3] [4 5 6]])

(defn funcion-keys-1
  [a]
  (keys a))
(defn funcion-keys-2
  [a]
  (keys a))
(defn funcion-keys-3
  [a]
  (keys a))

(funcion-keys-1 {})
(funcion-keys-2 nil)
 (funcion-keys-3 {5 4})

(defn funcion-max-1
  [a]
  (max a))
(defn funcion-max-2
  [a b c]
  (max a b c))
(defn funcion-max-3
  [a b c d]
  (max a b c d))

(funcion-max-1 5)
(funcion-max-2 1 2 3)
(funcion-max-3 44 55 88 77)

(defn funcion-merge-1
  [a]
  (merge a))
(defn funcion-merge-2
  [a b]
  (merge a b))
(defn funcion-merge-3
  [a]
  (merge a))

(funcion-merge-1 {:a 1 :b 2 :c 3})
(funcion-merge-2 {:a 1 :b 2 :c 3} {:b 9 :d 4})
(funcion-merge-1 nil )

(defn funcion-min-1
  [a]
  (min a))
(defn funcion-min-2
  [a b c]
  (min a b c))
(defn funcion-min-3
  [a b c d]
  (min a b c d))

(funcion-min-1 4)
(funcion-min-2 1 24 43)
(funcion-min-3 4 5 8 7)

(defn funcion-neg?-1
  [a]
  (neg? a))
(defn funcion-neg?-2
  [a]
  (neg? a))
(defn funcion-neg?-3
  [a]
  (neg? a))

(funcion-neg?-1 -1)
(funcion-neg?-2 0)
(funcion-neg?-3 1)

(defn funcion-nil?-1
  [a]
  (nil? a))
(defn funcion-nil?-2
  [a]
  (nil? a))
(defn funcion-nil?-3
  [a]
  (nil? a))

(funcion-nil?-1 nil)
(funcion-nil?-2 0)
(funcion-nil?-3 false)

(defn funcion-not-empty-1
  [a]
  (not-empty a))
(defn funcion-not-empty-2
  [a]
  (not-empty a))
(defn funcion-not-empty-3
  [a]
  (not-empty a))

(funcion-not-empty-1 [1])
(funcion-not-empty-1 [1 3 5])
(funcion-not-empty-1 [])

(defn funcion-nth-1
  [a b c]
  (nth a b c))
(defn funcion-nth-2
  [a b c]
  (nth a b c))
(defn funcion-nth-3
  [a b c]
  (nth a b c))

(funcion-nth-1 [] 0 "hola")
(funcion-nth-2 [8 4] 1 "agua")
(funcion-nth-3 [1 2 3] 77 845)

(defn funcion-odd?-1
  [a]
  (odd? a))
(defn funcion-odd?-2
  [a]
  (odd? a))
(defn funcion-odd?-3
  [a]
  (odd? a))

(funcion-odd?-1 1)
(funcion-odd?-2 0)
(funcion-odd?-3 5)

(defn funcion-partition-1
  [a b]
  (partition a b))
(defn funcion-partition-2
  [a b c]
  (partition a b c))
(defn funcion-partition-3
  [a b c d]
  (partition a b c d))

(funcion-partition-1 5 (range 20))
(funcion-partition-2 5 2 (range 50))
(funcion-partition-3 6 4 "a" (range 25))

(defn funcion-partition-all-1
  [a b]
  (partition a b))
(defn funcion-partition-all-2
  [a b]
  (partition a b))
(defn funcion-partition-all-3
  [a b c]
  (partition a b c))

(funcion-partition-all-1 2 [1 2 3 4 5 6 7 8 9])
(funcion-partition-all-2 4 [1 2 3 4 5 6 7 8 9])
(funcion-partition-all-3 2 4 [1 2 3 4 5 6 7 8 9])

(defn funcion-peek-1
  [a]
  (peek a))
(defn funcion-peek-2
  [a]
  (peek a))
(defn funcion-peek-3
  [a]
  (peek a))

(funcion-peek-1 [1 2 3])
(funcion-peek-1 [])
(funcion-peek-1 [1 2 3 4 5])

(defn funcion-pos?-1
  [a]
  (pos? a))
(defn funcion-pos?-2
  [a]
  (pos? a))
(defn funcion-pos?-3
  [a]
  (pos? a))

(funcion-pos?-1 1)
(funcion-pos?-2 0)
(funcion-pos?-3 -5)

(defn funcion-quot-1
  [a b]
  (quot a b))
(defn funcion-quot-2
  [a b]
  (quot a b))
(defn funcion-quot-3
  [a b]
  (quot a b))

(funcion-quot-1 6 2)
(funcion-quot-2 -5.9 2)
(funcion-quot-3 80 3)

(defn funcion-range-1
  [a]
  (range a))
(defn funcion-range-2
  [a b]
  (range a b)) 
(defn funcion-range-3
  [a b c]
  (range a b c))

(funcion-range-1 10)
(funcion-range-2 5 20)
(funcion-range-3 5 10 15)

(defn funcion-rem-1
  [a b]
  (rem a b))
(defn funcion-rem-2
  [a b]
  (rem a b))
(defn funcion-rem-3
  [a b]
  (rem a b))

(funcion-rem-1 5 10)
(funcion-rem-2 -50 65)
(funcion-rem-3 65 100)

(defn funcion-repeat-1
  [a b]
  (repeat a b))
(defn funcion-repeat-2
  [a b]
  (repeat a b))
(defn funcion-repeat-3
  [a b]
  (repeat a b))

(funcion-repeat-1 5 "x")
(funcion-repeat-2 8 "repeat")
(funcion-repeat-3 3 "funcion")

(defn funcion-replace-1
  [a b]
  (replace a b))
(defn funcion-replace-2
  [a b]
  (replace a b))
(defn funcion-replace-3
  [a b]
  (replace a b))
(funcion-replace-1 [\c] [1])
(funcion-replace-2 [\f] [8])
(funcion-replace-3 [\a] [2])

(defn funcion-rest-1
  [a]
  (rest a))
(defn funcion-rest-2
  [a]
  (rest a))
(defn funcion-rest-3
  [a]
  (rest a))
(funcion-rest-1 [ 1 2 3 4 5])
(funcion-rest-2 ["a" "b" "c"])
(funcion-rest-1 #{ 1 2 3 4})

(defn funcion-select-keys-1
  [a b]
  (select-keys a b))
(defn funcion-select-keys-2
  [a b]
  (select-keys a b))
(defn funcion-select-keys-3
  [a b]
  (select-keys a b))

(funcion-select-keys-1 {:a 2 :b 3} [:a])
(funcion-select-keys-1 [1 2 3 ] [1 2 0])
(funcion-select-keys-1 {:a "hola" :b "hi" :c "adioz"} [:a :b])

(defn funcion-shufle-1
  [a]
  (shuffle a))
(defn funcion-shufle-2
  [a]
  (shuffle a))
(defn funcion-shufle-3
  [a]
  (shuffle a))
(funcion-shufle-1 [5 8 9])
(funcion-shufle-2 [5 8 9 8 9 7])
(funcion-shufle-3 [8 9 4 6 2 3 4])

(defn funcion-sort-1
  [a]
  (sort a))
(defn funcion-sort-2
  [a]
  (sort a))
(defn funcion-sort-3
  [a]
  (sort a))
(funcion-sort-1 [5 8 4 6 1 3 5])
(funcion-sort-2 {:foo 5 :sdf 8 :ig 7})
(funcion-sort-3 [0.1 0.5 0.1])

(defn funcion-split-at-1
  [a b]
  (split-at a b))
(defn funcion-split-at-2
  [a b]
  (split-at a b))
(defn funcion-split-at-3
  [a b]
  (split-at a b))

(funcion-split-at-1 3 [1 2 3 4 5 6 7])
(funcion-split-at-1 4 [1 2 3 4 5 6 7 8])
(funcion-split-at-1 3 [1 2])

(defn funcion-str-1
  [a]
  (str a))
(defn funcion-str-2
  [a b]
  (str a b))
(defn funcion-str-3
  [a]
  (str a))

(funcion-str-1 1)
(funcion-str-2 1 2)
(funcion-str-3 nil)

(defn funcion-subs-1
  [a b]
  (subs a b))
(defn funcion-subs-2
  [a b c]
  (subs a b c))
(defn funcion-subs-3
  [a b c]
  (subs a b c))
(funcion-subs-1 "hola" 2)
(funcion-subs-2 "clojure" 1 3)
(funcion-subs-3 "funcion" 2 4)

(defn funcion-subvec-1
  [a b]
  (subvec a b))
(defn funcion-subvec-2
  [a b]
  (subvec a b))
(defn funcion-subvec-3
  [a b c]
  (subvec a b c))

(funcion-subvec-1 [1 2 3 4 5] 3)
(funcion-subvec-2 [1 2 3 4 5] 1)
(funcion-subvec-3 [1 2 3 4 5] 3 5)

(defn funcion-take-1
  [a b]
  (take a b))
(defn funcion-take-2
  [a b]
  (take a b))
(defn funcion-take-3
  [a b]
  (take a b))

(funcion-take-1 4 [1 2 3 4 5 6])
(funcion-take-2 -3 [1 2 3])
(funcion-take-3 0 [])

(defn funcion-true?-1
  [a]
  (true? a))
(defn funcion-true?-2
  [a]
  (true? a))
(defn funcion-true?-3
  [a]
  (true? a))

(funcion-true?-1 true)
(funcion-true?-2 1)
(funcion-true?-3 (= 1 1))

(defn funcion-vals-1
  [a]
  (vals a))
(defn funcion-vals-2
  [a]
  (vals a))
(defn funcion-vals-3
  [a]
  (vals a))

(funcion-vals-1 {:a "food" :b "hi"})
(funcion-vals-2 {})
(funcion-vals-3 {:e #{:m :f}, :c #{:f}, :b #{:c :f}, :d #{:m :f}, :a #{:c :f}})

(defn funcion-zero?-1
  [a]
  (zero? a))
(defn funcion-zero?-2
  [a]
  (zero? a))
(defn funcion-zero?-3
  [a]
  (zero? a))

(funcion-zero?-1 0)
(funcion-zero?-2 1.5156)
(funcion-zero?-3 0.2151)

